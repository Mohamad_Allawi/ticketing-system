<!DOCTYPE html>
<html>
<head>
    <title>New Ticket Created</title>
</head>
<body>
    <h1>New Ticket Created for {{$ticket['user']['name']}}</h1>

    <p><strong>Title:</strong> {{ $ticket['title'] }}</p>

    <p><strong>Description:</strong> {{ $ticket['description'] }}</p>

    <p><strong>Task Importance:</strong> {{ $ticket['task_importance'] }}</p>

    <p><strong>User Email:</strong> {{ $ticket['user']['email'] }}</p>
</body>
</html>
