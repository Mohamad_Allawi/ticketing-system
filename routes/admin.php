<?php

use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login',[LoginController::class ,'login'])->withoutMiddleware('role:Admin,admin');
Route::get('logout',[LoginController::class ,'logout']);


Route::group(['prefix'=>'tickets'],function(){
    Route::get('/',[TicketController::class ,'index']);
    Route::get('/with-pagination',[TicketController::class ,'indexWithPagination']);
    Route::post('{ticket}/change-status',[TicketController::class ,'changeStatus']);
    Route::post('{ticket}/add-comment',[TicketController::class ,'addCommentToTicket']);
});
