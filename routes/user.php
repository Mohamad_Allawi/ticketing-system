<?php

use App\Http\Controllers\User\Auth\LoginController;
use App\Http\Controllers\User\Auth\RegisterController;
use App\Http\Controllers\User\TicketController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::post('register',[RegisterController::class ,'register'])->withoutMiddleware('role:user,user');
Route::post('login',[LoginController::class , 'login'])->withoutMiddleware('role:user,user');
Route::get('logout',[LoginController::class ,'logout']);



Route::group(['prefix'=> 'tickets'],function(){
    Route::get('/',[TicketController::class ,'index']);
    Route::post('/',[TicketController::class ,'store']);
    Route::get('/{ticket}',[TicketController::class ,'show'])->name('ticket.show');
});
