<?php

namespace App\Jobs;

use App\Mail\SendNotificationAdmin;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class sendNotificationToAdmin implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $ticket , $admin ;
    public function __construct($ticket ,$admin)
    {
        $this->ticket = $ticket ;
        $this->admin = $admin ;
    }


    public function handle()
    {
        Mail::to($this->admin['email'])->send(new SendNotificationAdmin($this->ticket));
    }
}
