<?php

namespace App\Jobs;

use App\Mail\SendNotificationUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendNotificationToUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


   public $user , $ticket ;

    public function __construct($user , $ticket)
    {
        $this->user = $user ;
        $this->ticket = $ticket ;
    }


    public function handle()
    {
        Mail::to($this->user['email'])->send(new SendNotificationUser($this->ticket));
    }
}
