<?php

namespace App\Enum ;

enum TicketStatus:string {
    const CLOSED = 'Closed' ;
    const IN_PROGRESS = 'in_progress' ;
    const PENDING = 'Pending' ;
}
