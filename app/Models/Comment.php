<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $fillable = ['admin_id' ,'ticket_id','content'];

    protected $hidden = ['updated_at'];

    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i' ,
        'updated_at' => 'datetime:Y-m-d H:i'
    ];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function ticket(){
        return $this->belongsTo(Ticket::class);
    }

    
}
