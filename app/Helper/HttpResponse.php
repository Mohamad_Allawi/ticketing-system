<?php

if (!function_exists('success')) {
    function success($message): \Illuminate\Http\Response
    {
        return response([
            'success' => true,
            'message' => $message,
        ], 200);
    }
}

if (!function_exists('failure')) {
    function failure($message, $status): \Illuminate\Http\Response
    {
        return response([
            'success' => false,
            'message' => $message,
        ], $status);
    }
}


if (!function_exists('returnData')) {
    function returnData($key, $value, $pagination = null, $message = ""): \Illuminate\Http\Response
    {
        $response = [
            'success' => true,
            'message' => $message,
        ];

        if(is_array($key) && is_array($value)){
            for($i=0;$i<sizeof($key);$i++){
                $response[$key[$i]] = $value[$i];
            }
        }else{
            $response[$key] = $value ;
        }

        if ($pagination !== null) {
            $response['pagination'] = $pagination;
        }

        return response($response, 200);
    }
}
if(! function_exists('pagination_collection')){
    function pagination_collection($collection){
        return  $pagination_data = [
            'current_page' => $collection->currentPage(),
            'next_page_url' => $collection->nextPageUrl(),
            'prev_page_url' => $collection->previousPageUrl(),
            'first_page_url' => $collection->url(1),
            'last_page_url' => $collection->url($collection->lastPage()),
            'per_page' => $collection->perPage(),
            'total' => $collection->total(),
        ];

    };
}
