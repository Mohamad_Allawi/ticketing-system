<?php

namespace App\Repositories ;

use App\Interfaces\UserTicketInterface;
use App\Models\Ticket;

class UserTicketRepository implements UserTicketInterface {

    public function getAllTicketsByUser($user)
    {
        return Ticket::byUser($user)
            ->with('media','comments.admin')
            ->latest()
            ->get();
    }


    public function createTicket($data)
    {
        return Ticket::create($data);
    }


}
