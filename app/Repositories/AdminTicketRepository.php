<?php

namespace App\Repositories ;

use App\Interfaces\AdminTicketInterface;
use App\Models\Comment;
use App\Models\Ticket;

class AdminTicketRepository implements AdminTicketInterface {

    public function getAllTickets()
    {
        return Ticket::with('media','user','comments.admin')
            ->withFilter()
            ->withSort()
            ->get();
    }

    public function getAllTicketsWithPagination(){
        return Ticket::with('media','user','comments.admin')
        ->withFilter()
        ->withSort()
        ->paginate();
    }

    public function addCommentToTicket($data)
    {
        return Comment::create($data);
    }

    public function ticketChangeStatus($ticket ,$status)
    {
        $ticket->status = $status ;
        $ticket->save();
        return $ticket ;
    }


}
