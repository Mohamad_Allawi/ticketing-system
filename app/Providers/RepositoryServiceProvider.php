<?php

namespace App\Providers;

use App\Interfaces\AdminTicketInterface;
use App\Interfaces\UserTicketInterface;
use App\Repositories\AdminTicketRepository;
use App\Repositories\UserTicketRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserTicketInterface::class ,UserTicketRepository::class);
        $this->app->bind(AdminTicketInterface::class ,AdminTicketRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
