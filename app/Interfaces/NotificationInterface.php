<?php

namespace App\Interfaces;

interface NotificationInterface
{

    public function sendToUser($user ,$ticket);

    public function sendToAdmin($ticket);
}
