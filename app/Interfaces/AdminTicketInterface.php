<?php

namespace App\Interfaces;

interface AdminTicketInterface
{

    public function getAllTickets();

    public function getAllTicketsWithPagination();

    public function addCommentToTicket($data);

    public function ticketChangeStatus($ticket, $status);

}
