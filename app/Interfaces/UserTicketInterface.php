<?php

namespace App\Interfaces ;

interface UserTicketInterface {

    public function getAllTicketsByUser($user);

    public function createTicket($data);

    // public function getTicketById($id);
}
