<?php

namespace App\Http\Requests\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|email' ,
            'password' => 'required|string|min:8'
        ];
    }

    public function failedValidation(Validator $validator){
        throw new HttpResponseException(failure($validator->errors(),422));
    }
}
