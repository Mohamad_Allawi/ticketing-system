<?php

namespace App\Http\Requests\Admin\Ticket;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class filterRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'search' => 'sometimes|string' ,
            'sort_by' => 'sometimes|in:created_at,task_importance',
            'sort_dir' => 'sometimes|in:DESC,ASC'
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(failure($validator->errors(), 422));
    }
}
