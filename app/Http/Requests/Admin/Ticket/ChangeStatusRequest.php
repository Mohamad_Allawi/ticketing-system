<?php

namespace App\Http\Requests\Admin\Ticket;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ChangeStatusRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'status' => 'required|in:Pending,in_progress,Closed'
        ];
    }


    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(failure($validator->errors(), 422));
    }
}
