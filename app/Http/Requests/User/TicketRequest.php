<?php

namespace App\Http\Requests\User;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;

class TicketRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'attachment' => 'required|file|mimes:pdf,png,jpeg,jpg,webp,csv,docx|max:4096',
            'task_importance' => 'required|in:Normal,Important,Urgent'
        ];
    }

    public function ticketFilteredData()
    {
        return Arr::except($this->validated(), ['attachment']);
    }


    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(failure($validator->errors(), 422));
    }
}
