<?php

namespace App\Http\Requests\User\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Arr;

class RegisterRequest extends FormRequest
{
   
    public function authorize()
    {
        return true;
    }

    
    public function rules()
    {
        return [
            'name' => 'required|string|min:6|max:100' ,
            'email' => 'required|email|unique:users,email',
            'phone' => 'required|numeric|digits:10',
            'password' => 'required|string|min:8' ,
            'confirm_password' => 'required|same:password'
        ];
    }

    public function filteredData(){
        return Arr::except($this->validated(),'password','confirm_password');
    }

    public function failedValidation(Validator $validator){
        throw new HttpResponseException(failure($validator->errors(),422));
    }
}
