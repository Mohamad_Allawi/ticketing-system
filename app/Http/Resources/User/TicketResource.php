<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Admin\CommentResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id ,
            'title' => $this->title ,
            'description' => $this->description ,
            'attachment' => $this->media->first()?->original_url ,
            'task_importance' => $this->task_importance ,
            'status' => $this->status ,
            'comments' => CommentResource::collection($this->comments)
        ];
    }
}
