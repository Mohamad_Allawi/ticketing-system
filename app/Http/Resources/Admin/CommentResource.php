<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'ticket_id' => $this->ticket_id,
            'admin_id' => $this->admin_id,
            'admin_name' => $this->admin?->name,
            'created_at' => $this->created_at
        ];
    }
}
