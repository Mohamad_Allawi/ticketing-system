<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TicketResource extends JsonResource
{
    
    public function toArray($request)
    {
        return [
            'id' => $this->id ,
            'title' => $this->title ,
            'description' => $this->description ,
            'attachment' => $this->media->first()?->original_url ,
            'task_importance' => $this->task_importance ,
            'status' => $this->status , 
            'user' => UserResource::make($this->user),
            'comments' => CommentResource::collection($this->comments)
        ];
    }
}
