<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(LoginRequest $request){
        $credentials = $request->only('email', 'password');

        if (!Auth::guard('admin-api')->attempt($credentials))
            return failure('The email or password is incorrect', 401);

        $admin = auth('admin-api')->user();

        $token = $admin->createToken('admin_token')->accessToken;
        return returnData('access_token', $token);
    }

    public function logout(){
        $admin = auth('admin')->user();
        $admin->token()->revoke();
        return success('logged out successfully');
    }
}
