<?php

namespace App\Http\Controllers\Admin;

use App\Classes\MailNotification;
use App\Enum\TicketStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Ticket\ChangeStatusRequest;
use App\Http\Requests\Admin\Ticket\CommentRequest;
use App\Http\Requests\Admin\Ticket\filterRequest;
use App\Http\Resources\Admin\TicketResource;
use App\Interfaces\AdminTicketInterface;
use App\Interfaces\NotificationInterface;
use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    private $ticketRepo , $notification;


    public function __construct(AdminTicketInterface $ticketRepo ,NotificationInterface $notification)
    {
        $this->ticketRepo = $ticketRepo;
        $this->notification = $notification ;
    }


    public function index(filterRequest $request)
    {
        $tickets = $this->ticketRepo->getAllTickets();
        return returnData('tickets', TicketResource::collection($tickets));
    }



    public function indexWithPagination(filterRequest $request)
    {
        $tickets = $this->ticketRepo->getAllTicketsWithPagination();
        $pagination_data = pagination_collection($tickets);
        return returnData('tickets', TicketResource::collection($tickets), $pagination_data);
    }


    public function changeStatus(Ticket $ticket, ChangeStatusRequest $request)
    {
        if ($request->status == $ticket->status)
            return failure("ticket status is already $ticket->status", 450);


        elseif (($request->status == TicketStatus::PENDING && $ticket->status == TicketStatus::IN_PROGRESS) ||
            ($request->status == TicketStatus::PENDING && $ticket->status == TicketStatus::CLOSED) ||
            ($request->status == TicketStatus::IN_PROGRESS && $ticket->status == TicketStatus::CLOSED)
        )
            return failure("Ticket status cannot be changed because the status is $ticket->status", 450);

        $ticket = $this->ticketRepo->ticketChangeStatus($ticket, $request->status);

        if ($ticket->status == TicketStatus::CLOSED)
             $this->notification->sendToUser($ticket->user ,$ticket);

        return success('Ticket status changed successfully');
    }


    public function addCommentToTicket(Ticket $ticket ,CommentRequest $request)
    {
        $data = [
            'content' => $request->content ,
            'admin_id' => auth('admin')->id(),
            'ticket_id' => $ticket->id
        ];

        $this->ticketRepo->addCommentToTicket($data);

        return success('comment added successfully');
    }
}
