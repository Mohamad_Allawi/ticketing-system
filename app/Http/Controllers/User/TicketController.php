<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\TicketRequest;
use App\Http\Resources\User\TicketResource;
use App\Interfaces\NotificationInterface;
use App\Interfaces\UserTicketInterface;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\URL;

class TicketController extends Controller
{
    public $ticketRepo ,$notification;
    public function __construct(UserTicketInterface $ticketRepo ,NotificationInterface $notification)
    {
        $this->ticketRepo = $ticketRepo ;
        $this->notification =$notification ;
    }

    public function index()
    {
       $tickets = $this->ticketRepo->getAllTicketsByUser(auth('user')->id());
       return returnData('tickets',TicketResource::collection($tickets));
    }



    public function store(TicketRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->ticketFilteredData();
            $data['user_id'] = auth('user')->id();

            $ticket = $this->ticketRepo->createTicket($data);
            $ticket->addMedia($request->attachment)->toMediaCollection('attachment', 'tickets');
            $ticket->load('user');

            $this->notification->sendToAdmin($ticket);
            $url = URL::temporarySignedRoute('ticket.show', now()->addHours(5), $ticket->id);

            DB::commit();
            return returnData('data', $url, null, 'Ticket created successfully. Thank you');
        } catch (\Exception $ex) {
            DB::rollBack();
            return $ex->getMessage();          /////////////////////////
            return failure('sorry , somethings went wrongs', 450);
        }
    }

    public function show(Ticket $ticket)
    {
        // dd($ticket->user_id);

        if(! Gate::allows('show-ticket',$ticket))
            abort(403);
        return returnData('ticket',TicketResource::make($ticket->load('comments.admin')));
    }
}
