<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        if (!Auth::guard('user-api')->attempt($credentials))
            return failure('The email or password is incorrect', 401);

        $user = auth('user-api')->user();
        // if (!$user->email_verified_at)
        //     return failure('please verify account and try again', 413);

        $role = $user->getRoleNames()[0];
        $token = $user->createToken($role . '_token')->accessToken;
        return returnData('access_token', $token);
    }

    public function logout()
    {
        $user = auth('user')->user();
        $user->token()->revoke();
        return success('logged out successfully');
    }
}
