<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(RegisterRequest $request){
        try {
            DB::beginTransaction();
            $data = $request->filteredData();
            $data['password'] = Hash::make($request->password);
            $user = User::create($data);
            $user->assignRole('user');
            // $this->service->sendOtp($client->load('user')->toArray());
            DB::commit();
            return success('Account has been successfully registered');
            // send verification code
        } catch (\Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
            return failure('somethings went wrongs', 450);
        }
    }
}
