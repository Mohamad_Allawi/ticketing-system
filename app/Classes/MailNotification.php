<?php

namespace App\Classes;


use App\Interfaces\NotificationInterface;
use App\Jobs\sendNotificationToAdmin;
use App\Jobs\SendNotificationToUser;
use App\Models\Admin;

class MailNotification implements NotificationInterface
{

    public function sendToUser($user, $ticket)
    {
        dispatch(new SendNotificationToUser($user->toArray(), $ticket->toArray()));
    }

    public function sendToAdmin($ticket)
    {
        $admins = Admin::get();
        foreach ($admins as $admin) {
            dispatch(new sendNotificationToAdmin($ticket->toArray(), $admin->toArray()));
        }
    }
}
