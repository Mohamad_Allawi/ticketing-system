<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Comment;
use App\Models\Ticket;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminIds = Admin::pluck('id')->toArray();
        $ticketIds = Ticket::pluck('id')->toArray();

       
        $commentData = [];

        for ($i = 1; $i <= 50; $i++) {
            $commentData[] = [
                'content' => 'This is a sample comment ' . $i . ' for a ticket.',
                'admin_id' => $adminIds[array_rand($adminIds)],
                'ticket_id' => $ticketIds[array_rand($ticketIds)],
            ];
        }


        foreach ($commentData as $data) {
            Comment::create($data);
        }
    }

}
