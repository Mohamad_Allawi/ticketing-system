<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{

    public function run()
    {
        $admin = Admin::create([
            'name' => 'Admin' ,
            'email' => 'mohamad99elawi@gmail.com' ,
            'phone' => '0999999999' ,
            'password' => Hash::make('123456789')
        ]);

        $admin->assignRole('admin');
    }
}
