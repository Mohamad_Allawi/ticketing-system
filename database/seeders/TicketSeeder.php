<?php

namespace Database\Seeders;

use App\Models\Ticket;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TicketSeeder extends Seeder
{
    
    public function run()
    {
        
          $userIds = User::pluck('id')->toArray();
          $status =  ['Pending', 'in_progress', 'Closed'];
          $task_importance =  ['Normal', 'Important', 'Urgent'] ;
          $ticketData = [];
  
          for ($i = 1; $i <= 50; $i++) {
              $ticketData[] = [
                  'title' => 'Sample Ticket ' . $i,
                  'description' => 'This is a sample ticket description for Ticket ' . $i,
                  'status' => $status[array_rand($status)],
                  'task_importance' => $task_importance[array_rand($task_importance)],
                  'user_id' => $userIds[array_rand($userIds)],
              ];
          }
  
          
          foreach ($ticketData as $data) {
             $ticket = Ticket::create($data);
             $ticket->addMediaFromUrl(asset('fake_attachment/1.jpg'))->toMediaCollection('attachment', 'tickets');
          }
    }
}
